package com.mobile.app.ws.service.impl;

import com.mobile.app.ws.io.entity.User;
import com.mobile.app.ws.io.repository.UserRepository;
import com.mobile.app.ws.service.UserService;
import com.mobile.app.ws.shared.Utils;
import com.mobile.app.ws.shared.dto.UserDto;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    UserRepository userRepository;
    Utils utils;
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDto createUser(UserDto userDto) {
        if (userRepository.findByEmail(userDto.getEmail()) != null) throw new RuntimeException("Record already exists");

        User user = new User();
        BeanUtils.copyProperties(userDto, user);

        user.setUserId(utils.generateUserId(30));
        user.setEncryptedPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));

        User savedUser = userRepository.save(user);

        UserDto value = new UserDto();
        BeanUtils.copyProperties(savedUser, value);

        return value;
    }

    @Override
    public UserDto getUser(String email) {
        User user = userRepository.findByEmail(email);

        if (user == null) throw new UsernameNotFoundException(email);

        UserDto value = new UserDto();
        BeanUtils.copyProperties(user, value);
        return value;
    }

    @Override
    public UserDto getUserByUserId(String id) {
        UserDto value = new UserDto();
        User user = userRepository.findByUserId(id);
        if (user == null) throw new UsernameNotFoundException(id);
        BeanUtils.copyProperties(user, value);
        return value;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);

        if (user == null) throw new UsernameNotFoundException(email);

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getEncryptedPassword(), new ArrayList<>());
    }
}