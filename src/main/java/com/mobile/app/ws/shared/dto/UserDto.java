package com.mobile.app.ws.shared.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    private Long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String emailVerificationToken;
    private Boolean emailVerificationStatus = false;
}
