package com.mobile.app.ws.io.repository;

import com.mobile.app.ws.io.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository  extends CrudRepository<User, Long> {
    User findByEmail(String email);
    User findByUserId(String id);
}
